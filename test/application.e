note
	description: "eZLIB application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	make
			-- Run application.
		local
			l_adler: ZLIB_ADLER_32
			l_special: SPECIAL [NATURAL_8]
			l_val: INTEGER_64
			a,b,c: INTEGER_64
			l_crc32: ZLIB_CRC32
			l_string: STRING
			i : INTEGER
			l_special2: SPECIAL [INTEGER_64]
			l_deflate: ZLIB_DEFLATE
		do
--			create l_deflate.make
			a := 60
			b := 13
			c := a.bit_not
			c := a.bit_xor (b)

			create l_crc32.make
			create  l_string.make_from_string ("[")

			from
			until
				i = l_crc32.crc_table.count
			loop
				l_string.append (l_crc32.crc_table.at (i).out)
				l_string.append (",")
				i := i + 1
			end

			create l_adler.make
			create l_special.make_empty (5)
			l_special.force (85,0)
			l_special.force (14,1)
			l_special.force (112,2)
			l_special.force (101,3)
			l_special.force (125,4)
			l_crc32.update_with_buffer (l_special, 0, 3)
			l_val := l_crc32.value


			create l_special2.make_empty (3)
			l_special2.force (-85,0)
			l_special2.force (1114,1)
			l_special2.force (112,2)

			l_adler.update_with_buffer (l_special, 0, l_special.count)
			l_val := l_adler.value

			l_val := l_adler.combine (10, 0, 1)
		end

end
