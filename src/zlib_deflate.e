note
	description: "Summary description for {ZLIB_DEFLATE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ZLIB_DEFLATE

create
	make

feature {NONE} -- Initializtion

	make (a_stream: ZLIB_STREAM)
		do
			stream := a_stream

			create dyn_ltree.make_empty ({ZLIB_CONSTANTS}.heap_size * 2)
			create dyn_dtree.make_empty ((2 * {ZLIB_CONSTANTS}.d_codes + 1) * 2 )
					-- distance tree.
			create bl_tree.make_empty ((2 * {ZLIB_CONSTANTS}.bl_codes + 1) * 2)
					-- Huffman tree for bit lengths
			initialize_config_table
			initialize_error_messages
		end

	initialize_config_table
		do
			create config_table.make_empty
			config_table.force (create {ZLIB_CONFIG}.make (0, 0, 0, 0, {ZLIB_CONSTANTS}.Stored), 1) -- Store only.
			config_table.force (create {ZLIB_CONFIG}.make (4, 4, 8, 4, {ZLIB_CONSTANTS}.Fast), 2) -- max speed, no lazy matches.
			config_table.force (create {ZLIB_CONFIG}.make (4, 5, 16, 8, {ZLIB_CONSTANTS}.Fast), 3)
			config_table.force (create {ZLIB_CONFIG}.make (4, 6, 32, 32, {ZLIB_CONSTANTS}.Fast), 4)
			config_table.force (create {ZLIB_CONFIG}.make (4, 4, 16, 16, {ZLIB_CONSTANTS}.Slow), 5) -- Lazy matches.
			config_table.force (create {ZLIB_CONFIG}.make (8, 16, 32, 32, {ZLIB_CONSTANTS}.Slow), 6)
			config_table.force (create {ZLIB_CONFIG}.make (8, 16, 128, 128, {ZLIB_CONSTANTS}.Slow), 7)
			config_table.force (create {ZLIB_CONFIG}.make (8, 32, 128, 256, {ZLIB_CONSTANTS}.Slow), 8)
			config_table.force (create {ZLIB_CONFIG}.make (32, 128, 258, 1024, {ZLIB_CONSTANTS}.Slow), 9)
			config_table.force (create {ZLIB_CONFIG}.make (32, 258, 258, 4096, {ZLIB_CONSTANTS}.Slow), 10) -- max compression.
		end

	initialize_error_messages
		do
			create error_messages.make (9)
			error_messages.force ("need dictionary",{ZLIB_CONSTANTS}.z_need_dict)
			error_messages.force ("stream end", {ZLIB_CONSTANTS}.z_stream_end)
			error_messages.force ("", {ZLIB_CONSTANTS}.z_ok)
			error_messages.force ("file error", {ZLIB_CONSTANTS}.z_errno)
			error_messages.force ("stream error", {ZLIB_CONSTANTS}.z_stream_error)
			error_messages.force ("data error", {ZLIB_CONSTANTS}.z_data_error)
			error_messages.force ("insufficient memory", {ZLIB_CONSTANTS}.z_mem_error)
			error_messages.force ("buffer error", {ZLIB_CONSTANTS}.z_buf_error)
			error_messages.force ("incompatible version", {ZLIB_CONSTANTS}.z_version_error)
		end

feature -- Access

	stream: ZLIB_STREAM
			-- Reference to this zlib stream.

	status: INTEGER
			-- Current status.

	pending_buffer: detachable SPECIAL [NATURAL_8]
			-- Output pending.

	pending_buffer_size: INTEGER
			-- Size of pending buffer.

	pending_out: INTEGER
			-- Next pending byte to output to the stream.

	pending: INTEGER
			-- Number of bytes in the pending buffer.

	wrap: INTEGER
			-- default value 1.

	data_type: INTEGER
			-- 	UNKNOWN, BINARY or ASCII

	method: INTEGER
			-- STORED (for zip only) or DEFLATED

	last_flush: INTEGER
			-- value of flush param for previous deflate call.

	window_size: INTEGER
			-- LZ77 window size (32K by default)

	window_bits: INTEGER
			-- log2(winwdow_size)  (8..16)

	window_mask: INTEGER
			-- window_size - 1

	window: detachable SPECIAL [NATURAL_8]
			--  Sliding window. Input bytes are read into the second half of the window,
			--  and move to the first half later to keep a dictionary of at least wSize
			--  bytes. With this organization, matches are limited to a distance of
			--  Window_size-MAX_MATCH bytes, but this ensures that IO is always
			--  performed with a length multiple of the block size. Also, it limits
			--   the window size to 64K, which is quite useful on MSDOS.
			--  To do: use the user input buffer as sliding window.

	current_window_size: INTEGER
			--  Actual size of window: 2*window_size, except when the user input buffer
			--  is directly used as sliding window.

	prev: detachable SPECIAL [INTEGER_16]
			--  Link to older string with same hash index. To limit the size of this
			--  array to 64K, this link is maintained only for the last 32K strings.
			--  An index in this array is thus a window index modulo 32K.

	head: detachable SPECIAL [INTEGER_16]
			-- Heads of the hash chains or NIL.

	ins_h: INTEGER
			-- hash index of string to be inserted

	hash_size: INTEGER
			-- number of elements in hash table

	hash_bits: INTEGER
			-- log2(hash_size)

	hash_mask: INTEGER
			-- hash_size-1

	hash_shift: INTEGER
			--  Number of bits by which ins_h must be shifted at each input
			--  step. It must be such that after MIN_MATCH steps, the oldest
			--  byte no longer takes part in the hash key, that is:
			--  hash_shift * MIN_MATCH >= hash_bits



	block_start: INTEGER
			--  Window position at the beginning of the current output block. Gets
			--  negative when the window is moved backwards.


	match_length: INTEGER
			-- length of best match

	prev_match: INTEGER
			-- previous match

	match_available: INTEGER
			--	set if previous match exists

	str_start: INTEGER
			--  start of string to insert

	match_start: INTEGER
			-- start of matching string

	lookahead: INTEGER
			-- number of valid bytes ahead in window

	prev_length: INTEGER
			-- Length of the best match at previous step. Matches not greater than this
			-- are discarded. This is used in the lazy match evaluation.

	max_chain_length: INTEGER
			-- To speed up deflation, hash chains are never searched beyond this
			-- length.  A higher limit improves compression ratio but degrades the speed.

	max_lazy_match: INTEGER
			-- Attempt to find a better match only when the current match is strictly	
			-- smaller than this value. This mechanism is used only for compression
			-- levels >= 4.


	level: INTEGER
			-- compression level (1..9)
			--! Insert new strings in the hash table only if the match length is not
			--! greater than this length. This saves time but degrades compression.
			--! max_insert_length is used only for compression levels <= 3.

	strategy: INTEGER
			-- favor or force Huffman coding

	good_match: INTEGER
			-- Use a faster search when the previous match is longer than this.

	nice_match: INTEGER
			-- Stop searching when current match exceeds this.


	dyn_ltree: SPECIAL [INTEGER_16]
			-- literal and length tree.

	dyn_dtree: SPECIAL [INTEGER_16]
			-- distance tree.

	bl_tree: SPECIAL [INTEGER_16]
			-- Huffman tree for bit lengths.

--  Tree l_desc=new Tree();  // desc for literal tree
--  Tree d_desc=new Tree();  // desc for distance tree
--  Tree bl_desc=new Tree(); // desc for bit length tree

--  // number of codes at each bit length for an optimal tree
--  short[] bl_count=new short[MAX_BITS+1];
--  // working area to be used in Tree#gen_codes()
--  short[] next_code=new short[MAX_BITS+1];

--  // heap used to build the Huffman trees
--  int[] heap=new int[2*L_CODES+1];

--  int heap_len;               // number of elements in the heap
--  int heap_max;               // element of largest frequency
--  // The sons of heap[n] are heap[2*n] and heap[2*n+1]. heap[0] is not used.
--  // The same heap array is used to build all trees.

--  // Depth of each subtree used as tie breaker for trees of equal frequency
--  byte[] depth=new byte[2*L_CODES+1];

--  byte[] l_buf;               // index for literals or lengths */

--  // Size of match buffer for literals/lengths.  There are 4 reasons for
--  // limiting lit_bufsize to 64K:
--  //   - frequencies can be kept in 16 bit counters
--  //   - if compression is not successful for the first block, all input
--  //     data is still in the window so we can still emit a stored block even
--  //     when input comes from standard input.  (This can also be done for
--  //     all blocks if lit_bufsize is not greater than 32K.)
--  //   - if compression is not successful for a file smaller than 64K, we can
--  //     even emit a stored file instead of a stored block (saving 5 bytes).
--  //     This is applicable only for zip (not gzip or zlib).
--  //   - creating new Huffman trees less frequently may not provide fast
--  //     adaptation to changes in the input data statistics. (Take for
--  //     example a binary file with poorly compressible code followed by
--  //     a highly compressible string table.) Smaller buffer sizes give
--  //     fast adaptation but have of course the overhead of transmitting
--  //     trees more frequently.
--  //   - I can't count above 4
--  int lit_bufsize;

--  int last_lit;      // running index in l_buf

--  // Buffer for distances. To simplify the code, d_buf and l_buf have
--  // the same number of elements. To use different lengths, an extra flag
--  // array would be necessary.

--  int d_buf;         // index of pendig_buf

--  int opt_len;        // bit length of current block with optimal trees
--  int static_len;     // bit length of current block with static trees
--  int matches;        // number of string matches in current block
--  int last_eob_len;   // bit length of EOB code for last block

--  // Output buffer. bits are inserted starting at the bottom (least
--  // significant bits).
--  short bi_buf;

--  // Number of valid bits in bi_buf.  All bits above the last valid bit
--  // are always zero.
--  int bi_valid;

--  GZIPHeader gheader = null;




feature {NONE} -- Implementation

	config_table: ARRAY [ZLIB_CONFIG]
			-- configuration table.

	error_messages: HASH_TABLE [STRING, INTEGER]
			-- Error messages.		

end
