note
	description: "Object used to compute the CRC-32 of a data stream."
	date: "$Date$"
	revision: "$Revision$"

class
	ZLIB_CRC32

inherit

	ZLIB_CHECKSUM

create
	make

feature {NONE} -- Initialization

	make
		do
			initialize_crc_table
		end


	initialize_crc_table
		local
			n: INTEGER
			c: INTEGER_64
			k: INTEGER
			l_j: INTEGER_64
		do
			create crc_table.make_empty (256)
			l_j := j
			from
			until
				n = 256
			loop
				c := n
				from
					k := 7
				until
					k < 0
				loop
					if ((c & 1) /= 0) then
						c := (l_j.bit_xor( (c & 0xFFFFFFFF) |>> 1)).to_integer_32
        			else
          				c := ((c & 0xFFFFFFFF) |>> 1)
					end
					k := k - 1
				end
				crc_table.force (c, n)
				n := n + 1
			end
		end

feature -- Change Element

	update_with_buffer (a_buffer: SPECIAL [NATURAL_8]; a_index: INTEGER; a_length: INTEGER)
			-- <Precursor>
		local
			c: INTEGER_64
			l_length: INTEGER
			l_index: INTEGER
			l_val : INTEGER
		do
			c := internal_value.bit_not
			from
				l_length := a_length
				l_index := a_index
			until
				l_length = 0
			loop
				c := crc_table[((c.bit_xor (a_buffer[l_index]))& 0xff).to_integer_32].bit_xor (((c & 0xFFFFFFFF) |>> 8 ))
				l_index := l_index + 1
				l_length := l_length - 1
			end
			internal_value := (c.bit_not).to_integer_32
		end

	value: INTEGER_64
			-- <Precursor>
		do
			Result := (internal_value.to_integer_64 & 0xFFFFFFFFFFFFFFFF)
		end

	reset
			-- <Precursor>
		do
			internal_value := 0
		ensure then
			reset_v: internal_value = 0
		end

	reset_with (a_value: INTEGER_64)
			-- <Precursor>
		do
			internal_value :=  (a_value & 0xFFFFFFFFFFFFFFFF).to_integer_32
		end

	duplicate: ZLIB_CHECKSUM
			-- <Precursor>
		do
			Result := Current.twin
		end


	crc_table: SPECIAL [INTEGER_64]
			-- table for a byte-wise 32-bit CRC calculation.



feature -- CRC32- Check

	combine (a_crc1: INTEGER_64; a_crc2: INTEGER_64; a_len2: INTEGER_64): INTEGER_64
			-- Combine two CRC-32 check values into one.
		local
			l_row: INTEGER_64
			l_even: SPECIAL [INTEGER_64]
			l_odd: SPECIAL [INTEGER_64]
			i: INTEGER
			l_len2: INTEGER_64
			l_crc1: INTEGER_64
		do
			if a_len2 <= 0 then
					-- degenerate case (also disallow negative lengths)
				Result := a_crc1
			else
				create l_even.make_empty (gf2_dim)
				create l_odd.make_empty (gf2_dim)
					-- put operator for one zero bit in odd
				l_odd.force (j.to_integer_64, 0)  --CRC-32 polynomial	
				l_row := 1

				from
					i := 1
				until
					i = Gf2_dim
				loop
					l_odd.force (l_row, i)
					l_row := l_row |<< 1
					i := i + 1
				end

					-- put operator for two zero bits in even
				gf2_matrix_square(l_even, l_odd)

					-- put operator for four zero bits in odd
			    gf2_matrix_square(l_odd, l_even);

					-- apply len2 zeros to crc1 (first square will put the operator for one
					-- zero byte, eight zero bits, in even)

				from
					l_len2 := a_len2
					l_crc1 := a_crc1

				until
					l_len2 = 0
				loop
						-- apply zeros operator for this bit of len2
					gf2_matrix_square(l_even, l_odd);
					if ((l_len2 & 1) /= 0) then
						l_crc1 := gf2_matrix_times(l_even, l_crc1)
					end
					l_len2 := l_len2 |>> 1

					if l_len2 /= 0 then
							-- another iteration of the loop with odd and even swapped
						gf2_matrix_square(l_odd, l_even);
						if ((l_len2 & 1) /= 0) then
						 	l_crc1 := gf2_matrix_times(l_odd, l_crc1)
						end
						l_len2 := l_len2 |>> 1
					end
				end
				l_crc1 := l_crc1.bit_xor (a_crc2)
				Result := l_crc1
			end
		end


feature {NONE} -- Implementation

	gf2_matrix_times (a_mat: SPECIAL [INTEGER_64]; a_vec: INTEGER_64): INTEGER_64
				--
		local
			l_index: INTEGER
			l_vec: INTEGER_64
		do
			from
				l_vec := a_vec
			until
				l_vec = 0

			loop
				if ((l_vec & 1) /= 0) then
					Result := Result.bit_xor (a_mat[l_index])
				end
				l_vec := l_vec |>> 1
				l_index := l_index + 1
			end
		end


	gf2_matrix_square (a_square: SPECIAL [INTEGER_64]; a_mat: SPECIAL [INTEGER_64])
		local
			i: INTEGER
		do
			from

			until
				i = Gf2_dim
			loop
				a_square[i] := gf2_matrix_times (a_mat, a_mat[i])
				i := i + 1
			end
		end



feature {NONE} -- Implementation

 	Gf2_dim: INTEGER = 32;
 			-- length of CRC

	internal_value: INTEGER
			-- Internal crc value.

	j: INTEGER = 0xedb88320
			-- CRC-32 polynomial.

end
