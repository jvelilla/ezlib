note
	description: "Summary description for {ZLIB_CONFIG}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ZLIB_CONFIG

create
	make

feature {NONE} -- Initialization

	make (a_good_length: INTEGER; a_max_lazy: INTEGER; a_nice_length: INTEGER; a_max_chain: INTEGER; a_func: INTEGER)
			-- Values for max_lazy_match, good_match and max_chain_length, depending on
			-- the desired pack level (0..9). The values given below have been tuned to
			-- exclude worst case performance for pathological files. Better values may be
			-- found for specific files.
		do
			good_length := a_good_length
			max_lazy := a_max_lazy
			nice_length := a_nice_length
			max_chain := a_max_chain
			func := a_func
		end

feature -- Access

	good_length: INTEGER
			-- reduce lazy search above this match length.

	max_lazy: INTEGER
			-- do not perform lazy search above this match length.

	nice_length: INTEGER
			-- quit search above this match length.

	max_chain: INTEGER

	func: INTEGER
			-- Compress function.				
end

