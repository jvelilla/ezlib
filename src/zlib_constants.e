note
	description: "Summary description for {ZLIB_CONSTANTS}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	ZLIB_CONSTANTS


feature -- Compression

	Max_mem_level: INTEGER = 9

	Z_default_compression: INTEGER = 1

	Max_wbits: INTEGER = 15
		-- 32K LZ77 window

  	Def_mem_level: INTEGER = 8

	Stored: INTEGER = 0

	Fast: INTEGER = 1

  	Slow: INTEGER = 2


feature -- Return codes

	Z_ok: INTEGER = 0
	 		-- ok.

  	Z_stream_end: INTEGER = 1
  			-- stream end.

  	Z_need_dict: INTEGER = 2
  			-- need dictionary.

  	Z_errno: INTEGER = -1
  			-- File error.

  	Z_stream_error: INTEGER = -2
  			-- Stream error.

  	Z_data_error: INTEGER = -3
  			-- Data error.

  	Z_mem_error: INTEGER = -4
  			-- Insufficient memory.

  	Z_buf_error: INTEGER = -5
  			-- Buffer error.

  	Z_version_error: INTEGER = -6
  			-- Incompatible version.


feature -- Function prototypes

	Need_more: INTEGER = 0
  		--block not completed, need more input or more output.

 	Block_done: INTEGER = 1
	    -- block flush performed.

	Finished_started: INTEGER = 2
		-- finish started, need only more output at next deflate.

 	Finished_done: INTEGER = 3
		-- finish done, accept no more input or output.

feature -- Header

  	Preset_dict: INTEGER = 0x20
		-- preset dictionary flag in zlib header.

feature -- Compression strategy

	Z_filtered: INTEGER = 1

	Z_huffman_only: INTEGER = 2

	Z_rle: INTEGER = 3

	Z_fixed: INTEGER = 4

	Z_default_strategy: INTEGER = 0


feature -- Flush

	Z_no_flush: INTEGER = 0

	Z_partial_flush: INTEGER = 1

	Z_sync_flush: INTEGER = 2

	Z_full_flush: INTEGER = 3

	Z_finish: INTEGER = 4

	Z_block: INTEGER = 5


feature -- Stream status

  	Init_state: INTEGER = 42

	Extra_state: INTEGER = 69

	Name_state: INTEGER = 73

	Comment_state: INTEGER = 91

	Hcrc_state: INTEGER = 103

  	Busy_state: INTEGER = 113

  	Finish_state: INTEGER = 666


 feature -- Compression method

 	Z_deflated: INTEGER =  8
		-- The deflate compression method (the only one supported in this version).

feature --

 	Stored_block: INTEGER = 0

  	Static_trees: INTEGER = 1

  	Dyn_trees: INTEGER = 2


feature -- Data Type

	Z_binary: INTEGER = 0
	Z_text: INTEGER   = 1
	Z_ascii: INTEGER  = 1 -- For compatibilit with 1.2.2 and earlier.
	Z_unknown: INTEGER = 2


feature -- Deflate util

	Buf_size: INTEGER = 16
		-- Buf_size defined as 8*2

	Rep_3_6: INTEGER = 16
		-- repeat previous bit length 3-6 times (2 bits of repeat count)

	Repz_3_10: INTEGER = 17
		-- repeat a zero length 3-10 times  (3 bits of repeat count)

	Repz_11_138: INTEGER = 18
		--  repeat a zero length 11-138 times  (7 bits of repeat count)


	Min_match: INTEGER = 3

	Max_match: INTEGER = 258

	Min_lookahead: INTEGER = 262
			-- MIN_LOOKAHEAD=(MAX_MATCH+MIN_MATCH+1)

	Max_bits: INTEGER = 15

	D_codes: INTEGER = 30

	Bl_codes: INTEGER = 19

	Length_codes: INTEGER = 29

	Literals: INTEGER = 256

	L_codes: INTEGER = 286
		-- L_CODES=(LITERALS+1+LENGTH_CODES);

	Heap_size: INTEGER = 573
		-- HEAP_SIZE=(2*L_CODES+1)	

	End_block: INTEGER = 256
		-- end of block literal code

	Max_bl_bits: INTEGER = 7
		-- Bit length codes must not exceed MAX_BL_BITS bits.

end
