note
	description: "Interface Representing data checksum"
	date: "$Date$"
	revision: "$Revision$"

deferred class
	ZLIB_CHECKSUM

feature -- Change Element

	update_with_buffer (a_buffer: SPECIAL [NATURAL_8]; a_index: INTEGER; a_length: INTEGER)
			-- Updates the current checksum with the specified buffer `a_buffer'.
			-- starting at index `a_index' and the number of bytes to update `a_length'.
		deferred
		end

	value: INTEGER_64
			-- Checksum value.
		deferred
		end

	reset
			-- Reset the checksum to its initial value.
		deferred
		end

	reset_with (a_value: INTEGER_64)
			-- Reset tge checksum with the value `a_value'.
		deferred
		end

	duplicate: ZLIB_CHECKSUM
			-- Checksum copy
		deferred
		end

end

