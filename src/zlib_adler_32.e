note
	description: "Compute the Adler32 checksum of a data stream."
	date: "$Date$"
	revision: "$Revision$"

class
	ZLIB_ADLER_32

inherit

	ZLIB_CHECKSUM

create
	make

feature {NONE} -- Initialization

	make
		do
			s1 := DefaultS1
			s2 := DefaultS2
		ensure
			states_set: s1 = DefaultS1 and s2 = DefaultS2
		end

feature -- Change Element: Checksum



feature -- Change Element

	update_with_buffer (a_buffer: SPECIAL [NATURAL_8]; a_index: INTEGER; a_length: INTEGER)
			-- Updates the current checksum with the specified buffer `a_buffer'.
			-- starting at index `a_index' and the number of bytes to update `a_length'.
		local
			l_len1: INTEGER
			l_len2: INTEGER
			k: INTEGER
			l_length: INTEGER
			l_index: INTEGER
		do
			if a_length = 1 then
				s1 := s1 + a_buffer[(a_index)] & 0xff
				s2 := s2 + s1
				s1 := s1 \\ Base
				s2 := s2 \\ Base
			else
				l_len1 := a_length // Nmax
				l_len2 := a_length \\ Nmax
				from
					l_length := a_length
					l_index := a_index
				until
					l_len1 = 0
				loop
					k := Nmax
					l_length := l_length - k
					from

					until
						k = 0
					loop
						s1 := s1 + a_buffer[(l_index)] & 0xff
						s2 := s2 + s1
						l_index := l_index + 1
						k := k - 1
					end
					s1 := s1 \\ Base
					s2 := s2 \\ Base
					l_len1 := l_len1 - 1
				end

				k := l_len2
				l_length := l_length - k
				from

				until
					k = 0
				loop
					s1 := s1 + a_buffer[(l_index)] & 0xff
					s2 := s2 + s1
					l_index := l_index + 1
					k := k - 1
				end
				s1 := s1 \\ Base
				s2 := s2 \\ Base
			end
		end

	value: INTEGER_64
			-- Checksum value.
		do
			Result := ((s2 |<< 16) | s1)
		end

	reset
			-- Reset the checksum to its initial value.
		do
			s1 := DefaultS1
			s2 := DefaultS2
		ensure then
			states_set: s1 = DefaultS1 and s2 = DefaultS2
		end

	reset_with (a_value: INTEGER_64)
			-- Reset the checksum with the value `a_value'.
		do
			s1 := a_value & 0xffff
			s2 := (a_value |>> 16) & 0xffff
		end

	duplicate: ZLIB_CHECKSUM
			-- Checksum copy
		do
          Result := Current.twin
		end



	combine (a_adler1: INTEGER_64; a_adler2: INTEGER_64; a_length: INTEGER_64): INTEGER_64
			--
		local
			l_sum1: INTEGER_64
			l_sum2: INTEGER_64
			l_rem: INTEGER_64
			l_basel: INTEGER_64
		do
			l_basel := Base

			l_rem := a_length \\ l_basel
			l_sum1 := a_adler1 & 0xffff
			l_sum2 := l_rem * l_sum1

			l_sum2 := l_sum2 \\ l_basel
			l_sum1 := l_sum1 + (a_adler2 & 0xffff) + l_basel - 1;
			l_sum2 := l_sum2 + ((a_adler1 |>> 16) & 0xffff) + ((a_adler2 |>> 16) & 0xffff) + l_basel - l_rem;

			if (l_sum1 >= l_basel) then
				l_sum1 := l_sum1 - l_basel
			end
			if (l_sum1 >= l_basel) then
				l_sum1 := l_sum1 - l_basel
			end
			if (l_sum2 >= (l_basel |<< 1)) then
				l_sum2 := l_sum2 - (l_basel |<< 1)
			end
			if (l_sum2 >= l_basel) then
				l_sum2 := l_sum2 - l_basel
			end
			Result := l_sum1 | (l_sum2 |<< 16)
		end

feature {NONE} -- Implementation


	frozen Base: INTEGER = 65521
			-- largest prime smaller than 65536

	frozen Nmax: INTEGER = 5552
			-- NMAX is the largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1

	s1: INTEGER_64
 	s2: INTEGER_64

 	DefaultS1: INTEGER_64 = 1
 	DefaultS2: INTEGER_64 = 0


end
